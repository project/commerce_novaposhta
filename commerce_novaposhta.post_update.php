<?php

/**
 * @file
 * Post update functions for commerce_novaposhta.
 */

/**
 * @addtogroup updates-2.0.x-2.1.0
 * @{
 */

/**
 * Re-save all shipping methods with novaposhta data to update region.
 */
function commerce_novaposhta_post_update_add_shipping_method_region(&$sandbox = NULL) {
  $storage = \Drupal::entityTypeManager()
    ->getStorage('commerce_shipping_method');
  $shipping_methods_ids = $storage->getQuery()
    ->condition('plugin', 'novaposhta')
    ->accessCheck(FALSE)
    ->execute();
  /** @var \Drupal\commerce_shipping\Entity\ShippingMethodInterface[] $shipping_methods */
  $shipping_methods = $storage->loadMultiple($shipping_methods_ids);
  if (!$shipping_methods) {
    return;
  }
  /** @var \Drupal\commerce_novaposhta\Service\NovaposhtaManager $novaposhtaManager */
  $novaposhtaManager = \Drupal::service('commerce.novaposhta.manager');
  $regions = $novaposhtaManager->getRegions();
  foreach ($shipping_methods as $shipping_method) {
    // Work on the raw plugin item to avoid defaults being merged in.
    $plugin = $shipping_method->get('plugin')->first();
    $configuration = $plugin->target_plugin_configuration;
    $options = &$configuration['options'];
    if ($options['rate_type'] !== 'calculate') {
      continue;
    }
    $options['novaposhta']['region'] = $options['novaposhta']['city_data']['Area'];
    // Get region data for region value.
    $key = array_search($options['novaposhta']['region'], array_column($regions, 'Ref'));
    if ($key !== FALSE) {
      $options['novaposhta']['region_data'] = $regions[$key] ?? NULL;
    }
    $plugin->target_plugin_configuration = $configuration;
    $shipping_method->save();
  }
}

/**
 * Re-save all entities with novaposhta data to update region and region_data.
 */
function commerce_novaposhta_post_update_fill_region(&$sandbox = NULL) {
  if (!isset($sandbox['fields'])) {
    $sandbox['fields'] = \Drupal::state()->get('commerce_novaposhta_8900_processed');
    // No fields were updated.
    if (empty($sandbox['fields'])) {
      $sandbox['#finished'] = 1;
      return;
    }
    $sandbox['count'] = count($sandbox['fields']);
  }

  $field = array_pop($sandbox['fields']);
  $entity_type_id = $field[0];
  $field_name = $field[1];
  /** @var \Drupal\commerce_novaposhta\Service\NovaposhtaManager $novaposhtaManager */
  $novaposhtaManager = \Drupal::service('commerce.novaposhta.manager');
  $regions = $novaposhtaManager->getRegions();
  $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
  $query = $storage->getQuery()->exists($field_name . '.city_data');
  $query->condition($field_name . '.region_data', 'NULL', 'IS NULL');
  $query->accessCheck(FALSE);
  $entities = $storage->loadMultiple($query->execute());
  foreach ($entities as $entity) {
    foreach ($entity->{$field_name} as $address_novaposhta) {
      /** @var \Drupal\commerce_novaposhta\Plugin\Field\FieldType\NovaposhtaItem $address_novaposhta */
      $values = $address_novaposhta->getValue();
      $values['region'] = $values['city_data']['Area'];
      // Get region data for default region value.
      $key = array_search($values['region'], array_column($regions, 'Ref'));
      if ($key !== FALSE) {
        $values['region_data'] = $regions[$key] ?? NULL;
      }
      $address_novaposhta->setValue($values);
    }
    $entity->save();
  }

  $sandbox['#finished'] = empty($sandbox['fields']) ? 1 : ($sandbox['count'] - count($sandbox['fields'])) / $sandbox['count'];
  return t('Updated the region and region_data of each novaposhta address.');
}

/**
 * @} End of "addtogroup updates-2.0.x-2.1.0".
 */
