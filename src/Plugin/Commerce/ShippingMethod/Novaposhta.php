<?php

namespace Drupal\commerce_novaposhta\Plugin\Commerce\ShippingMethod;

use Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface;
use Drupal\commerce_price\Price;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\commerce_shipping\PackageTypeManagerInterface;
use Drupal\commerce_shipping\Plugin\Commerce\ShippingMethod\ShippingMethodBase;
use Drupal\commerce_shipping\ShippingRate;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\state_machine\WorkflowManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Novaposhta shipping method.
 *
 * @CommerceShippingMethod(
 *   id = "novaposhta",
 *   label = @Translation("Novaposhta"),
 *   services = {
 *     "Doors" = @Translation("Novaposhta Doors"),
 *     "Warehouse" = @Translation("Novaposhta Warehouse"),
 *     "Postomat" = @Translation("Novaposhta Postomat"),
 *   }
 * )
 */
class Novaposhta extends ShippingMethodBase {

  /**
   * Flat rate, ignoring product weight and dimensions.
   */
  const RATETYPE_FLAT = 'flat';

  /**
   * Calculate rate based on product weight and dimensions.
   */
  const RATETYPE_CALCULATE = 'calculate';

  /**
   * Default currency.
   */
  const DEFAULT_CURRENCY = 'UAH';

  /**
   * Novaposhta manager.
   *
   * @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   */
  protected $novaposhtaManager;

  /**
   * Constructs a new Novaposhta object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\commerce_shipping\PackageTypeManagerInterface $package_type_manager
   *   The package type manager.
   * @param \Drupal\state_machine\WorkflowManagerInterface $workflow_manager
   *   The workflow manager.
   * @param \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface $novaposhta_manager
   *   The Novaposhta manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, PackageTypeManagerInterface $package_type_manager, WorkflowManagerInterface $workflow_manager, NovaposhtaManagerInterface $novaposhta_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $package_type_manager, $workflow_manager);
    $this->novaposhtaManager = $novaposhta_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.commerce_package_type'),
      $container->get('plugin.manager.workflow'),
      $container->get('commerce.novaposhta.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'options' => [
        'rate_type' => static::RATETYPE_FLAT,
        'rate_amount' => NULL,
        'novaposhta' => [],
      ],
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['options'] = [
      '#type' => 'details',
      '#title' => $this->t('Novaposhta options'),
      '#description' => $this->t('Additional options for Novaposhta'),
      '#weight' => 9,
      '#open' => TRUE,
    ];

    $form['options']['rate_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Rate type'),
      '#description' => $this->t('Select rate type. "Flat rate" will be flat rate for all deliveries, "Calculate" will send request to Novaposhta to get rate based on product weight and dimensions.'),
      '#options' => [
        static::RATETYPE_FLAT => $this->t('Flat rate'),
        static::RATETYPE_CALCULATE => $this->t('Calculate'),
      ],
      '#default_value' => $this->configuration['options']['rate_type'],
    ];

    $form['options']['rate_amount'] = [
      '#type' => 'details',
      '#title' => $this->t('Shipping services rates'),
      '#description' => $this->t('Enter "0" for Free delivery'),
      '#states' => [
        'visible' => [
          'select[name$="[novaposhta][options][rate_type]"]' => ['value' => static::RATETYPE_FLAT],
        ],
        'required' => [
          'select[name$="[novaposhta][options][rate_type]"]' => ['value' => static::RATETYPE_FLAT],
        ],
      ],
    ];

    $services = $this->services ?? $this->getServices();
    foreach ($services as $service) {
      $amount = $this->configuration['options']['rate_amount'][$service->getId()] ?? NULL;
      // Check if $amount in compatible format.
      if (isset($amount) && !isset($amount['number'], $amount['currency_code'])) {
        $amount = NULL;
      }
      $form['options']['rate_amount'][$service->getId()] = [
        '#type' => 'commerce_price',
        '#title' => $this->t('Rate amount: @service', ['@service' => $service->getLabel()]),
        '#default_value' => $amount,
      ];
    }

    $form['options']['novaposhta'] = [
      '#type' => 'details',
      '#title' => $this->t('Novaposhta address'),
      '#description' => $this->t('Select your Shop Novaposhta address for the rate calculation.'),
      '#states' => [
        'visible' => [
          'select[name$="[novaposhta][options][rate_type]"]' => ['value' => static::RATETYPE_CALCULATE],
        ],
        'required' => [
          'select[name$="[novaposhta][options][rate_type]"]' => ['value' => static::RATETYPE_CALCULATE],
        ],
      ],
    ];

    if ($this->novaposhtaManager->isConfigured()) {
      $form['options']['novaposhta']['address'] = [
        '#type' => 'address_novaposhta',
        '#default_value' => $this->configuration['options']['novaposhta'],
      ];
    }
    else {
      $api_link = Link::createFromRoute(
        $this->t('Novaposhta API'),
        'commerce_novaposhta.novaposhta_config_form',
        [],
        ['attributes' => ['target' => '_blank']]
      )->toString();

      $form['options']['novaposhta']['address'] = [
        '#prefix' => '<p>',
        '#suffix' => '</p>',
        '#markup' => $this->t('You should configure @api_link before selecting address.', [
          '@api_link' => $api_link,
        ]),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::validateConfigurationForm($form, $form_state);

    $values = $form_state->getValue($form['#parents']);

    $services = array_filter($values['services']) ?? [];
    if ($values['options']['rate_type'] === static::RATETYPE_FLAT) {
      foreach ($services as $service) {
        if (empty($values['options']['rate_amount'][$service]['number']) && !is_numeric($values['options']['rate_amount'][$service]['number'])) {
          $element = &$form['options']['rate_amount'][$service];
          $form_state->setError($element, $this->t('@name should not be empty', ['@name' => $element['#title']]));
        }
      }
    }

    if ($values['options']['rate_type'] === static::RATETYPE_CALCULATE) {
      if (!$this->novaposhtaManager->isConfigured()) {
        $element = &$form['options']['rate_type'];
        $form_state->setError($element, $this->t('You should configure Novaposhta API before use this @name', ['@name' => $element['#title']]));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['options']['rate_type'] = $values['options']['rate_type'];
      $this->configuration['options']['rate_amount'] = $values['options']['rate_amount'];
      $this->configuration['options']['novaposhta'] = $values['options']['novaposhta']['address'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function calculateRates(ShipmentInterface $shipment) {
    // Calculate Flat rates if enabled.
    if ($this->isFlatRate()) {
      $rates = [];
      foreach (array_keys($this->getServices()) as $service) {
        $rates[] = new ShippingRate([
          'shipping_method_id' => $this->parentEntity->id(),
          'service' => $this->services[$service],
          'amount' => Price::fromArray($this->configuration['options']['rate_amount'][$service]),
        ]);
      }

      return $rates;
    }

    // Calculates rates from Novaposhta API.
    $availableRates = [];
    if ($shipment->getShippingProfile()->get('address_novaposhta')->isEmpty()) {
      return [];
    }

    if (empty($shipment->getPackageType())) {
      $shipment->setPackageType($this->getDefaultPackageType());
    }

    $rates = $this->novaposhtaManager->getRates($shipment, $this->configuration);

    foreach ($rates as $key => $rate) {
      $price = new Price($rate, static::DEFAULT_CURRENCY);
      // Replace CostWarehouse so only WarehouseDoors and WarehouseWarehouse
      // rates are available.
      $key = str_replace('CostWarehouse', '', $key);
      if (array_key_exists($key, $this->getServices())) {
        $availableRates[$rate] = new ShippingRate([
          'shipping_method_id' => $this->parentEntity->id(),
          'service' => $this->services[$key],
          'amount' => $price,
        ]);
      }
    }

    // Sort by price ASC.
    ksort($availableRates);
    return $availableRates;
  }

  /**
   * Determine if flat rate should be used.
   *
   * @return bool
   *   TRUE if is flat rate, FALSE otherwise.
   */
  protected function isFlatRate() {
    $options = $this->configuration['options'];

    return (!empty($options['rate_type']) && $options['rate_type'] === static::RATETYPE_FLAT);
  }

}
