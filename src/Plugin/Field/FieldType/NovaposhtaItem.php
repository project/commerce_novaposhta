<?php

namespace Drupal\commerce_novaposhta\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;

/**
 * Plugin implementation of the 'novaposhta' field type.
 *
 * @FieldType(
 *   id = "novaposhta",
 *   label = @Translation("Novaposhta"),
 *   category = @Translation("Address"),
 *   description = @Translation("Field containing Novaposhta address"),
 *   default_widget = "novaposhta_default",
 *   default_formatter = "novaposhta_default"
 * )
 */
class NovaposhtaItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'max_length' => 36,
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public static function mainPropertyName() {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['region'] = DataDefinition::create('string')
      ->setLabel(t('Region'));

    $properties['city'] = DataDefinition::create('string')
      ->setLabel(t('City'));

    $properties['warehouse'] = DataDefinition::create('string')
      ->setLabel(t('Warehouse'));

    $properties['region_data'] = MapDataDefinition::create()
      ->setLabel(t('Serialized Region Data'));

    $properties['city_data'] = MapDataDefinition::create()
      ->setLabel(t('Serialized City Data'));

    $properties['warehouse_data'] = MapDataDefinition::create()
      ->setLabel(t('Serialized Warehouse Data'));

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = [
      'columns' => [
        'region' => [
          'description' => 'The region ref.',
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'city' => [
          'description' => 'The city ref.',
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'warehouse' => [
          'description' => 'The warehouse ref.',
          'type' => 'varchar',
          'length' => (int) $field_definition->getSetting('max_length'),
        ],
        'region_data' => [
          'description' => 'Serialized Region Data.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
        'city_data' => [
          'description' => 'Serialized City Data.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
        'warehouse_data' => [
          'description' => 'Serialized Warehouse Data.',
          'type' => 'blob',
          'not null' => TRUE,
          'serialize' => TRUE,
        ],
      ],
    ];

    return $schema;
  }

  /**
   * {@inheritdoc}
   */
  public function getConstraints() {
    $constraints = parent::getConstraints();

    if ($max_length = $this->getSetting('max_length')) {
      $constraint_manager = \Drupal::typedDataManager()
        ->getValidationConstraintManager();
      $constraints[] = $constraint_manager->create('ComplexData', [
        'city' => [
          'Length' => [
            'max' => $max_length,
            'maxMessage' => $this->t('%name: may not be longer than @max characters.', [
              '%name' => $this->getFieldDefinition()->getLabel(),
              '@max' => $max_length,
            ]),
          ],
        ],
      ]);
    }

    return $constraints;
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $random = new Random();
    $values['region'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    $values['city'] = $random->word(mt_rand(1, $field_definition->getSetting('max_length')));
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $city = $this->get('city')->getValue();
    $warehouse = $this->get('warehouse')->getValue();

    return $city === NULL || $city === '' || $warehouse === NULL || $warehouse === '';
  }

}
