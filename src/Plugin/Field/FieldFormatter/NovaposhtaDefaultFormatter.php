<?php

namespace Drupal\commerce_novaposhta\Plugin\Field\FieldFormatter;

use Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'novaposhta_default' formatter.
 *
 * @FieldFormatter(
 *   id = "novaposhta_default",
 *   label = @Translation("Novaposhta"),
 *   field_types = {
 *     "novaposhta"
 *   }
 * )
 */
class NovaposhtaDefaultFormatter extends FormatterBase {

  /**
   * NovaposhtaManager definition.
   *
   * @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   */
  protected $novaposhtaManager;

  /**
   * Constructs a NovaposhtaDefaultFormatter object.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface $novaposhta_manager
   *   Novaposhta Manager service.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, NovaposhtaManagerInterface $novaposhta_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->novaposhtaManager = $novaposhta_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('commerce.novaposhta.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      // Implement default settings.
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [
      // Implement settings form.
    ] + parent::settingsForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];

    foreach ($items as $delta => $item) {
      $region = $item->region_data['Description'] ?? NULL;
      $city = $item->city_data['Description'] ?? NULL;
      $warehouse = $item->warehouse_data['Description'] ?? NULL;

      $elements[$delta]['region'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $region,
        '#attributes' => [
          'class' => ['novaposhta-region-item'],
        ],
      ];

      $elements[$delta]['city'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $city,
        '#attributes' => [
          'class' => ['novaposhta-city-item'],
        ],
      ];

      $elements[$delta]['warehouse'] = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $warehouse,
        '#attributes' => [
          'class' => ['novaposhta-warehouse-item'],
        ],
      ];
    }

    return $elements;
  }

}
