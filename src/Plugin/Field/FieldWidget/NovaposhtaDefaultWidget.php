<?php

namespace Drupal\commerce_novaposhta\Plugin\Field\FieldWidget;

use Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'novaposhta_default' widget.
 *
 * @FieldWidget(
 *   id = "novaposhta_default",
 *   module = "commerce_novaposhta",
 *   label = @Translation("Novaposhta"),
 *   field_types = {
 *     "novaposhta"
 *   }
 * )
 */
class NovaposhtaDefaultWidget extends WidgetBase {

  /**
   * NovaposhtaManager definition.
   *
   * @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   */
  protected $novaposhtaManager;

  /**
   * Constructs a NovaposhtaDefaultWidget object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface $novaposhta_manager
   *   Novaposhta Manager.
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, array $third_party_settings, NovaposhtaManagerInterface $novaposhta_manager) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $third_party_settings);
    $this->novaposhtaManager = $novaposhta_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('commerce.novaposhta.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $value = $items[$delta]->toArray();

    $element = [
      '#type' => 'address_novaposhta',
      '#default_value' => $value,
    ] + $element;

    return $element;
  }

}
