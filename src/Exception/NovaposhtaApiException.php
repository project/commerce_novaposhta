<?php

namespace Drupal\commerce_novaposhta\Exception;

/**
 * Thrown when the Novaposhta API makes an invalid request.
 *
 * A request is invalid due to missing or invalid parameters.
 */
class NovaposhtaApiException extends \InvalidArgumentException {}
