<?php

namespace Drupal\commerce_novaposhta\Form;

use Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Novaposhta Config Form.
 */
class NovaposhtaConfigForm extends ConfigFormBase {

  /**
   * NovaposhtaManager definition.
   *
   * @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   */
  protected $novaposhtaManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->novaposhtaManager = $container->get('commerce.novaposhta.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'commerce_novaposhta.novaposhta',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'novaposhta_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('commerce_novaposhta.novaposhta');

    $form['api_information'] = [
      '#type' => 'details',
      '#title' => $this->t('API information'),
      '#description' => $this->isConfigured() ? $this->t('Update your Novaposhta API information.') : $this->t('Fill in your Novaposhta API information.'),
      '#weight' => $this->isConfigured() ? 10 : -10,
      '#open' => !$this->isConfigured(),
    ];

    $form['api_information']['api_key'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('API key'),
      '#description' => $this->t('Enter your Novaposhta API key. More info how to create API key check <a href="@href" target="_blank">here</a>', ['@href' => 'https://developers.novaposhta.ua/']),
      '#default_value' => $config->get('api_key'),
    ];

    $form['api_information']['api_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API request Url'),
      '#description' => $this->t('Novaposhta API request Url'),
      '#default_value' => $config->get('api_url') ?: NovaposhtaManagerInterface::API_URL,
      '#disabled' => TRUE,
    ];

    $language_options = [
      'ua' => $this->t('Ukrainian'),
    ];
    $default_language = $config->get('language') ?? NULL;
    $default_language = $default_language && array_keys($language_options, $default_language) ? $default_language : NULL;
    $form['language'] = [
      '#type' => 'select',
      '#title' => $this->t('Language'),
      '#options' => $language_options,
      '#description' => $this->t('Select Novaposhta language. By default Ukrainian language will be used.'),
      '#default_value' => $default_language ?: $this->novaposhtaManager->getLanguage(),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $api_key = $form_state->getValue('api_key');
    $api_url = $form_state->getValue('api_url');
    $this->novaposhtaManager->setApiKey($api_key);
    $this->novaposhtaManager->setApiUrl($api_url);
    $result = $this->novaposhtaManager->searchSettlements('Львів', 1);
    if (!$result) {
      $form_state->setErrorByName('api_key', $this->t('API key is not valid'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('commerce_novaposhta.novaposhta')
      ->set('api_key', $form_state->getValue('api_key'))
      ->set('api_url', $form_state->getValue('api_url'))
      ->set('language', $form_state->getValue('language'))
      ->save();
  }

  /**
   * Determine if we have the minimum information to connect to Novaposhta.
   *
   * @return bool
   *   TRUE if there is enough information to connect, FALSE otherwise.
   */
  protected function isConfigured() {
    $config = $this->config('commerce_novaposhta.novaposhta');
    return !empty($config->get('api_key'));
  }

}
