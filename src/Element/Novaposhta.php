<?php

namespace Drupal\commerce_novaposhta\Element;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element\FormElement;

/**
 * Provides a Novaposhta form element.
 *
 * Usage example:
 * @code
 * $form['novaposhta'] = [
 *   '#type' => 'address_novaposhta',
 *   '#default_value' => [
 *     'region' => '71508138-9b87-11de-822f-000c2965ae0e',
 *     'city' => 'a9522a7e-eaf5-11e7-ba66-005056b2fc3d',
 *     'warehouse' => 'a9522a9d-eaf5-11e7-ba66-005056b2fc3d',
 *     'region_data' => [],
 *     'city_data' => [],
 *     'warehouse_data' => [],
 *   ],
 * ];
 * @endcode
 *
 * @FormElement("address_novaposhta")
 */
class Novaposhta extends FormElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#input' => TRUE,
      '#multiple' => FALSE,
      '#default_value' => NULL,
      '#process' => [
        [$class, 'processElement'],
        [$class, 'processAjaxForm'],
        [$class, 'processGroup'],
      ],
      '#element_validate' => [
        [$class, 'validateElement'],
      ],
      '#after_build' => [
        [$class, 'clearValues'],
      ],
      '#pre_render' => [
        [$class, 'preRenderGroup'],
      ],
      '#theme_wrappers' => ['container'],
    ];
  }

  /**
   * Ensures all keys are set on the provided value.
   *
   * @param array $value
   *   The value.
   *
   * @return array
   *   The modified value.
   */
  public static function applyDefaults(array $value) {
    $properties = [
      'region',
      'region_data',
      'city',
      'city_data',
      'warehouse',
      'warehouse_data',
    ];
    foreach ($properties as $property) {
      if (!isset($value[$property])) {
        $value[$property] = NULL;
      }
    }

    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public static function valueCallback(&$element, $input, FormStateInterface $form_state) {
    // Ensure both the default value and the input have all keys set.
    if (is_array($input)) {
      $input = static::applyDefaults($input);
    }
    $element['#default_value'] = (array) $element['#default_value'];
    $element['#default_value'] = static::applyDefaults($element['#default_value']);

    return is_array($input) ? $input : $element['#default_value'];
  }

  /**
   * Processes Novaposhta form element.
   *
   * @param array $element
   *   The form element to process.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $complete_form
   *   The complete form structure.
   *
   * @return array
   *   The processed element.
   *
   * @throws \Exception
   */
  public static function processElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    /** @var \Drupal\commerce_novaposhta\Service\NovaposhtaManager $novaposhtaManager */
    $novaposhtaManager = \Drupal::service('commerce.novaposhta.manager');

    $values = $element['#value'];

    // Extract Region options.
    $regions = $novaposhtaManager->getRegions();
    $region_options = $novaposhtaManager->extractRegionOptions($regions);

    if (empty($values['region']) && empty($values['region_data']) && !empty($values['city_data'])) {
      $values['region'] = $values['city_data']['Area'];
    }

    $city_options = [];
    $warehouse_options = [];

    if ($values['region']) {
      // Get region data for default region value.
      $key = array_search($values['region'], array_column($regions, 'Ref'));
      if ($key !== FALSE) {
        $values['region_data'] = $regions[$key] ?? NULL;
      }

      // Extract City options.
      $cities = $novaposhtaManager->getCities($values['region']);
      $city_options = $novaposhtaManager->extractCityOptions($cities);

      // Reset City and City data when the region changes.
      if ($values['city'] && !array_key_exists($values['city'], $city_options)) {
        $values['city_data'] = NULL;
        $values['city'] = NULL;
      }

      if ($values['city']) {
        // Get city data for default city value.
        $key = array_search($values['city'], array_column($cities, 'Ref'));
        if ($key !== FALSE) {
          $values['city_data'] = $cities[$key] ?? NULL;
        }

        // Extract Warehouse options.
        $warehouses = $novaposhtaManager->getWarehouses($values['city']);
        $warehouse_options = $novaposhtaManager->extractWarehouseOptions($warehouses);

        if ($values['warehouse']) {
          // Get warehouse data for default warehouse value.
          $key = array_search($values['warehouse'], array_column($warehouses, 'Ref'));
          if ($key !== FALSE) {
            $values['warehouse_data'] = $warehouses[$key] ?? NULL;
          }
        }
      }
    }

    // Reset Warehouse and Warehouse data when the city changes.
    if ($values['warehouse'] && !array_key_exists($values['warehouse'], $warehouse_options)) {
      $values['warehouse_data'] = !empty($warehouses) ? reset($warehouses) : NULL;
      $values['warehouse'] = $values['warehouse_data']['Ref'] ?? NULL;
    }

    $id_prefix = implode('-', $element['#parents']);
    $wrapper_id = Html::getUniqueId($id_prefix . '-ajax-wrapper');
    $element += [
      '#prefix' => '<div id="' . $wrapper_id . '">',
      '#suffix' => '</div>',
    ];

    $element['region'] = [
      '#type' => 'select',
      '#title' => t('Region'),
      '#options' => $region_options,
      '#empty_option' => t('- None -'),
      '#default_value' => $values['region'] ?? NULL,
      '#required' => $element['#required'],
      '#maxlength' => 36,
      '#parents' => array_merge($element['#parents'], ['region']),
      '#ajax' => [
        'callback' => [get_called_class(), 'ajaxRefresh'],
        'disable-refocus' => TRUE,
        'event' => 'change',
        'wrapper' => $wrapper_id,
      ],
    ];
    if (!$element['#required']) {
      $element['region']['#empty_value'] = '';
    }

    if (!empty($values['region'])) {
      $element['city'] = [
        '#type' => 'select',
        '#title' => t('City'),
        '#options' => $city_options,
        '#empty_option' => t('- None -'),
        '#default_value' => $values['city'] ?? NULL,
        '#required' => $element['#required'],
        '#maxlength' => 36,
        '#parents' => array_merge($element['#parents'], ['city']),
        '#ajax' => [
          'callback' => [get_called_class(), 'ajaxRefresh'],
          'disable-refocus' => TRUE,
          'event' => 'change',
          'wrapper' => $wrapper_id,
        ],
        '#access' => !empty($values['region']),
      ];
      if (!$element['#required']) {
        $element['city']['#empty_value'] = '';
      }
    }

    if (!empty($values['city'])) {
      $element['warehouse'] = [
        '#type' => 'select',
        '#title' => t('Warehouse'),
        '#options' => $warehouse_options,
        '#default_value' => $values['warehouse'] ?? NULL,
        '#required' => $element['#required'],
        '#maxlength' => 36,
        '#parents' => array_merge($element['#parents'], ['warehouse']),
        '#access' => !empty($values['city']),
        '#needs_validation' => FALSE,
      ];
      if (!$element['#required']) {
        $element['warehouse']['#empty_value'] = '';
      }
    }

    // Save city and warehouse data to element state.
    $element_state = [
      'region_data' => $values['city_data'],
      'city_data' => $values['city_data'],
      'warehouse_data' => $values['warehouse_data'],
    ];
    static::setElementState($element['#parents'], $form_state, $element_state);

    return $element;
  }

  /**
   * Validate form element.
   */
  public static function validateElement(array &$element, FormStateInterface $form_state, array &$complete_form) {
    // Retrieve city and warehouse data from element state and set as form
    // values, so it will be available after form submit.
    $element_state = static::getElementState($element['#parents'], $form_state);
    if (!empty($element_state)) {
      foreach ($element_state as $key => $value) {
        $form_state->setValue(array_merge($element['#parents'], [$key]), $value);
      }
    }
  }

  /**
   * Clears the warehouse form values when the city changes.
   *
   * Implemented as an #after_build callback because #after_build runs before
   * validation, allowing the values to be cleared early enough to prevent the
   * "Illegal choice" error.
   *
   * @param array $element
   *   The form element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   Form element.
   */
  public static function clearValues(array $element, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (!$triggering_element) {
      return $element;
    }

    $keys = [
      'city' => [
        'warehouse',
      ],
    ];
    $triggering_element_name = end($triggering_element['#parents']);
    if (isset($keys[$triggering_element_name])) {
      $input = &$form_state->getUserInput();
      foreach ($keys[$triggering_element_name] as $key) {
        if (isset($element[$key])) {
          $parents = array_merge($element['#parents'], [$key]);
          NestedArray::setValue($input, $parents, '');
          $element[$key]['#value'] = NULL;
        }
      }
    }

    return $element;
  }

  /**
   * Ajax callback when the city changes.
   *
   * @param array $form
   *   The Form elements.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The form element.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    return NestedArray::getValue($form, array_slice($triggering_element['#array_parents'], 0, -1));
  }

  /**
   * Gets the element state.
   *
   * @param array $parents
   *   The element parents.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   *
   * @return array
   *   The element state.
   */
  public static function getElementState(array $parents, FormStateInterface $form_state) {
    $parents = array_merge(['element_state', '#parents'], $parents);
    return NestedArray::getValue($form_state->getStorage(), $parents);
  }

  /**
   * Sets the element state.
   *
   * @param array $parents
   *   The element parents.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param array $element_state
   *   The element state.
   */
  public static function setElementState(array $parents, FormStateInterface $form_state, array $element_state) {
    $parents = array_merge(['element_state', '#parents'], $parents);
    NestedArray::setValue($form_state->getStorage(), $parents, $element_state);
  }

}
