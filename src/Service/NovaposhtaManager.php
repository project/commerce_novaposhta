<?php

namespace Drupal\commerce_novaposhta\Service;

use Drupal\commerce_novaposhta\Exception\NovaposhtaApiException;
use Drupal\commerce_shipping\Entity\ShipmentInterface;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\RequestOptions;
use Psr\Log\LoggerInterface;

/**
 * Provides Novaposhta Manager.
 */
class NovaposhtaManager implements NovaposhtaManagerInterface {

  /**
   * Api key.
   *
   * @var string
   */
  protected $key;

  /**
   * API Url.
   *
   * @var string
   */
  protected $apiUrl;

  /**
   * Language.
   *
   * @var string
   */
  protected $language;

  /**
   * Http client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The cache backend.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new NovaposhtaManager object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \GuzzleHttp\ClientInterface $client
   *   The http client.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   The cache backend.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ClientInterface $client,
    ModuleHandlerInterface $module_handler,
    CacheBackendInterface $cache_backend,
    LoggerInterface $logger
  ) {
    $this->config = $config_factory->get('commerce_novaposhta.novaposhta');
    $this->httpClient = $client;
    $this->moduleHandler = $module_handler;
    $this->cache = $cache_backend;
    $this->logger = $logger;

    // Set default configurations for API.
    $this->setDefaults();
  }

  /**
   * Set default configurations for Novaposhta API.
   */
  public function setDefaults() {
    $this->setApiKey($this->config->get('api_key'));
    $this->setApiUrl($this->config->get('api_url'));
    $this->setLanguage($this->config->get('language'));
  }

  /**
   * {@inheritdoc}
   */
  public function setApiKey(string $api_key): NovaposhtaManagerInterface {
    $this->key = $api_key;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiKey(): string {
    return $this->key;
  }

  /**
   * {@inheritdoc}
   */
  public function isConfigured(): bool {
    return (bool) $this->key;
  }

  /**
   * {@inheritdoc}
   */
  public function setApiUrl(string $url = NULL): NovaposhtaManagerInterface {
    $this->apiUrl = $url ?: static::API_URL;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getApiUrl(): string {
    return $this->apiUrl ?: static::API_URL;
  }

  /**
   * {@inheritdoc}
   */
  public function setLanguage(string $language): NovaposhtaManagerInterface {
    $this->language = $language ?: static::LANGUAGE;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getLanguage(): string {
    return $this->language;
  }

  /**
   * {@inheritdoc}
   */
  public function request(string $model, string $method, array $parameters = []): array {
    $uri = $this->getApiUrl();
    $data = [
      'apiKey' => $this->key,
      'modelName' => $model,
      'calledMethod' => $method,
      'language' => $this->getLanguage(),
    ];
    if ($parameters) {
      $data['methodProperties'] = $parameters;
    }

    // Allow other modules to alter request data.
    $this->moduleHandler->alter('commerce_novaposhta_request_data', $data);

    $request_options[RequestOptions::BODY] = Json::encode($data);
    $request_options[RequestOptions::HEADERS]['Content-Type'] = 'application/json';

    $result = [];
    try {
      $response = $this->httpClient->request('POST', $uri, $request_options);

      if ($response->getStatusCode() == 200) {
        $content = $response->getBody()->getContents();
        $result = Json::decode($content);

        // Allow other modules to alter result data.
        $this->moduleHandler->alter('commerce_novaposhta_result_data', $result, $data);

        if (!empty($result['errors'])) {
          throw new NovaposhtaApiException(reset($result['errors']));
        }
      }
    }
    catch (RequestException | NovaposhtaApiException $e) {
      $this->logger->error($e->getMessage(), $data);
    }

    return !empty($result['success']) ? $result : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRates(ShipmentInterface $shipment, array $config): array {
    // All rates returned by Novaposhta API ready to return from this call.
    $availableRates = [];

    if ($shipment->getShippingProfile()->get('address_novaposhta')->isEmpty()) {
      return [];
    }

    $novaposhta = $shipment->getShippingProfile()
      ->get('address_novaposhta')
      ->first();

    $params = [
      'CitySender' => $config['options']['novaposhta']['city'],
      'CityRecipient' => $novaposhta->get('city')->getValue(),
      'Weight' => $shipment->getWeight()->convert('kg')->getNumber(),
      // @todo set ServiceType or receive all rate types.
      'ServiceType' => '',
      'Cost' => $shipment->getTotalDeclaredValue()->getNumber(),
      'CargoType' => 'Cargo',
      'SeatsAmount' => 1,
    ];

    $response = $this->request(static::INTERNET_DOCUMENT_MODEL, static::GET_DOCUMENT_PRICE_METHOD, $params);

    if ($response) {
      $availableRates = reset($response['data']);
    }

    return $availableRates;
  }

  /**
   * {@inheritdoc}
   */
  public function searchSettlements(string $city, int $limit = 1): array {
    $results = $this->request(static::ADDRESS_MODEL, static::SEARCH_SETTLEMENTS_METHOD, [
      'CityName' => $city,
      'Limit' => $limit,
    ]);
    return $results ? $results['data'] : [];
  }

  /**
   * {@inheritdoc}
   */
  public function getRegions(): array {
    $cid = 'commerce_novaposhta:get_regions';

    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {
      $results = $this->request(static::ADDRESS_MODEL, static::GET_AREAS_METHOD);
      $data = $results ? $results['data'] : [];

      if ($data) {
        $this->cache->set($cid, $data, time() + 84600);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function extractRegionOptions(array $regions): array {
    $options = [];

    foreach ($regions as $region) {
      // Skip Crimea.
      if ($region['Ref'] === '71508128-9b87-11de-822f-000c2965ae0e') {
        continue;
      }
      $options[$region['Ref']] = $this->language === static::LANGUAGE ? $region['Description'] : '';
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getCities(string $region): array {
    $cid = sprintf('commerce_novaposhta:get_cities:%s', $region);

    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {
      $results = $this->request(static::ADDRESS_MODEL, static::GET_CITIES_METHOD);
      $data = [];
      if ($results && !empty($results['data'])) {
        foreach ($results['data'] as $city) {
          if ($city['Area'] == $region) {
            $data[] = $city;
          }
        }
      }

      if ($data) {
        $this->cache->set($cid, $data, time() + 84600);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function extractCityOptions(array $cities): array {
    $options = [];

    foreach ($cities as $city) {
      $options[$city['Ref']] = $this->language === static::LANGUAGE ? $city['Description'] : $city['DescriptionRu'];
    }

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function getWarehouses(string $city_ref): array {
    $cid = 'commerce_novaposhta:get_warehouses:' . $city_ref;

    if ($cache = $this->cache->get($cid)) {
      $data = $cache->data;
    }
    else {
      $results = $this->request(static::ADDRESS_MODEL, static::GET_WAREHOUSES_METHOD, [
        'CityRef' => $city_ref,
      ]);
      $data = $results ? $results['data'] : [];

      if ($data) {
        $this->cache->set($cid, $data, time() + 84600);
      }
    }

    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public function extractWarehouseOptions(array $warehouses): array {
    $options = [];

    foreach ($warehouses as $warehouse) {
      $options[$warehouse['Ref']] = $this->language === static::LANGUAGE ? $warehouse['Description'] : $warehouse['DescriptionRu'];
    }

    return $options;
  }

}
