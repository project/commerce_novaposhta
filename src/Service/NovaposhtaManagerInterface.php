<?php

namespace Drupal\commerce_novaposhta\Service;

use Drupal\commerce_shipping\Entity\ShipmentInterface;

/**
 * Provides an interface for Novaposhta Manager.
 */
interface NovaposhtaManagerInterface {

  /**
   * Novaposhta models to use in 'modelName' in API requests.
   */
  const ADDRESS_MODEL = 'Address';

  const INTERNET_DOCUMENT_MODEL = 'InternetDocument';

  /**
   * Novaposhta methods to use in 'calledMethod' in API requests.
   */
  const GET_CITIES_METHOD = 'getCities';

  const GET_STREET_METHOD = 'getStreet';

  const GET_AREAS_METHOD = 'getAreas';

  const GET_SETTLEMENTS_METHOD = 'getSettlements';

  const GET_WAREHOUSES_METHOD = 'getWarehouses';

  const GET_DOCUMENT_LIST_METHOD = 'getDocumentList';

  const GET_DOCUMENT_METHOD = 'getDocument';

  const GET_DOCUMENT_PRICE_METHOD = 'getDocumentPrice';

  const FIND_NEAREST_WAREHOUSES_METHOD = 'findNearestWarehouse';

  const SEARCH_SETTLEMENTS_METHOD = 'searchSettlements';

  /**
   * Default Novaposhta Language.
   */
  const LANGUAGE = 'ua';

  /**
   * Default Api Url for requests.
   */
  const API_URL = 'https://api.novaposhta.ua/v2.0/json/';

  /**
   * Fetch rates from the Novaposhta API.
   *
   * @param \Drupal\commerce_shipping\Entity\ShipmentInterface $shipment
   *   Shipping profile.
   * @param array $config
   *   Shipping method configuration.
   *
   * @return array
   *   The available rates as an array.
   */
  public function getRates(ShipmentInterface $shipment, array $config): array;

  /**
   * Set API key.
   *
   * @param string $api_key
   *   API key.
   *
   * @return \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   *   Self instance.
   */
  public function setApiKey(string $api_key): NovaposhtaManagerInterface;

  /**
   * Get API Key.
   *
   * @return string
   *   API Key.
   */
  public function getApiKey(): string;

  /**
   * Check if Novaposhta API configured.
   *
   * @return bool
   *   True if configured, otherwise False.
   */
  public function isConfigured(): bool;

  /**
   * Do request.
   *
   * @param string $model
   *   Request Model.
   * @param string $method
   *   Request Method.
   * @param array $parameters
   *   Request parameters.
   *
   * @return array
   *   Response result.
   */
  public function request(string $model, string $method, array $parameters = []): array;

  /**
   * Set API Url.
   *
   * @param string $url
   *   The Api Url for request to Novaposhta.
   *
   * @return \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   *   Self instance.
   */
  public function setApiUrl(string $url): NovaposhtaManagerInterface;

  /**
   * Get API URL.
   *
   * @return string
   *   API Url.
   */
  public function getApiUrl(): string;

  /**
   * Set language.
   *
   * @param string $language
   *   The language code, Novaposhta supports only 'ua' language.
   *
   * @return \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface
   *   Self instance.
   */
  public function setLanguage(string $language): NovaposhtaManagerInterface;

  /**
   * Get language.
   *
   * @return string
   *   Language.
   */
  public function getLanguage(): string;

  /**
   * Search settlements.
   *
   * @param string $city
   *   City name.
   * @param int $limit
   *   Results limit.
   *
   * @return array
   *   Settlements.
   */
  public function searchSettlements(string $city, int $limit = 1): array;

  /**
   * Regions vocabulary.
   *
   * @return array
   *   Cities.
   */
  public function getRegions(): array;

  /**
   * Extract regions options according to language.
   *
   * @param array $regions
   *   The regions from API response.
   *
   * @return array
   *   The region options [region_ref => region_description].
   */
  public function extractRegionOptions(array $regions): array;

  /**
   * Cities vocabulary.
   *
   * @param string $region
   *   Region reference.
   *
   * @return array
   *   Cities.
   */
  public function getCities(string $region): array;

  /**
   * Extract city options according to language.
   *
   * @param array $cities
   *   The cities from API response.
   *
   * @return array
   *   The city options [city_ref => city_description].
   */
  public function extractCityOptions(array $cities): array;

  /**
   * Warehouses vocabulary by city Ref.
   *
   * @param string $city_ref
   *   City Ref.
   *
   * @return array
   *   Warehouses.
   */
  public function getWarehouses(string $city_ref): array;

  /**
   * Extract warehouse options according to language.
   *
   * @param array $warehouses
   *   The warehouses from API response.
   *
   * @return array
   *   The warehouse options [warehouse_ref => warehouse_description].
   */
  public function extractWarehouseOptions(array $warehouses): array;

}
