<?php

namespace Drupal\Tests\commerce_novaposhta\Unit;

use Drupal\commerce_novaposhta\Service\NovaposhtaManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use GuzzleHttp\ClientInterface;
use Psr\Log\LoggerInterface;

/**
 * Provides test for Novaposhta Manager.
 *
 * @package Drupal\Tests\commerce_novaposhta\Unit
 *
 * @group commerce_novaposhta
 */
class NovaposhtaManagerTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $container = new ContainerBuilder();
    $config_mock = $this->createMock(ImmutableConfig::class);
    $config_mock->expects($this->any())->method('get')->willReturn('');
    $config_factory_mock = $this->createMock(ConfigFactoryInterface::class);
    $config_factory_mock->expects($this->any())
      ->method('get')
      ->willReturn($config_mock);
    $client_mock = $this->createMock(ClientInterface::class);
    $module_handler = $this->createMock(ModuleHandlerInterface::class);
    $cache_mock = $this->createMock(CacheBackendInterface::class);
    $logger_mock = $this->createMock(LoggerInterface::class);
    $novaposhta_manager = new NovaposhtaManager($config_factory_mock, $client_mock, $module_handler, $cache_mock, $logger_mock);
    $container->set('commerce.novaposhta.manager', $novaposhta_manager);
    \Drupal::setContainer($container);
  }

  /**
   * Checks if the service is created in the Drupal context.
   */
  public function testNovaposhtaManager() {
    $this->assertNotNull(\Drupal::service('commerce.novaposhta.manager'));
  }

  /**
   * Test setting api key.
   */
  public function testConfigured() {
    /** @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface $manager */
    $manager = \Drupal::service('commerce.novaposhta.manager');
    $this->assertFalse($manager->isConfigured());
    $manager->setApiKey('do-what-you-love');
    $this->assertTrue($manager->isConfigured());
  }

  /**
   * Test default language.
   */
  public function testGetLanguage() {
    /** @var \Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface $manager */
    $manager = \Drupal::service('commerce.novaposhta.manager');
    $this->assertEquals('ua', $manager->getLanguage());
  }

}
