<?php

namespace Drupal\Tests\commerce_novaposhta\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Provides test for Novaposhta Config Form.
 *
 * @package Drupal\Tests\commerce_novaposhta\Functional
 *
 * @group commerce_novaposhta
 */
class NovaposhtaConfigFormTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'commerce_novaposhta',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $permissions = [
      'administer commerce novaposhta configuration',
      'view the administration theme',
      'access administration pages',
    ];
    $this->adminUser = $this->drupalCreateUser($permissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test config form.
   */
  public function testConfigForm() {
    $this->drupalGet('admin/config/services/commerce-novaposhta');
    $edit = [
      'api_key' => $this->randomGenerator->string(),
    ];
    $this->assertSession()->pageTextContains('API information');
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('API key is not valid');
    $edit = [
      'api_key' => '0f9a64ed21d311ecf4a4c908aa323b17',
    ];
    $this->submitForm($edit, 'Save configuration');
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
  }

}
