<?php

namespace Drupal\Tests\commerce_novaposhta\Kernel;

use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;

/**
 * Provides test for Novaposhta Item.
 *
 * @package Drupal\Tests\commerce_novaposhta\Kernel
 *
 * @group commerce_novaposhta
 */
class NovaposhtaItemTest extends EntityKernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce_novaposhta',
  ];

  /**
   * The test entity.
   *
   * @var \Drupal\entity_test\Entity\EntityTest
   */
  protected $testEntity;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();

    $field_storage = FieldStorageConfig::create([
      'field_name' => 'field_novaposhta',
      'entity_type' => 'entity_test',
      'type' => 'novaposhta',
      'cardinality' => 1,
    ]);
    $field_storage->save();

    $field = FieldConfig::create([
      'field_name' => 'field_novaposhta',
      'entity_type' => 'entity_test',
      'bundle' => 'entity_test',
    ]);
    $field->save();

    $entity = EntityTest::create([
      'name' => 'Test',
    ]);
    $entity->save();
    $this->testEntity = $entity;
  }

  /**
   * Test field item.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testNovaposhta() {
    $field_data = [
      'region' => 'Lviv region',
      'city' => 'Lviv',
      'warehouse' => 'underground_metro',
      'region_data' => [],
      'city_data' => [],
      'warehouse_data' => [],
    ];
    $this->testEntity->set('field_novaposhta', $field_data);
    $this->testEntity->save();

    $this->testEntity = $this->reloadEntity($this->testEntity);
    $stored_value = $this->testEntity->get('field_novaposhta')->getValue();
    $this->assertEquals($stored_value[0], $field_data);
  }

}
