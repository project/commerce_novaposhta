# Commerce Novaposhta Shipping

Integration of the Novaposhta API 2.0 with Commerce Shipping. Provides
estimated shipping costs and warehouses
selection of Novaposhta services. This module should be used by those
that want to provide Novaposhta shipping to their customers.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_novaposhta).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_novaposhta).

## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers

## Requirements

This module requires:

- [Novaposhta account](http://new.novaposhta.ua/) and
  [API Key](https://new.novaposhta.ua/dashboard/settings/developers)
- [Commerce Shipping](https://www.drupal.org/project/commerce_shipping) module

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Add dimensions and weight fields (new field types via the Physical module)
   to all shippable product types.
2. Populate dimensions and weight fields for all products.
3. Configure Novaposhta settings in Administration » Configuration » Web
   services » Commerce
   Novaposhta (`/admin/config/services/commerce-novaposhta`).
4. Add Novaposhta shipping method (`/admin/commerce/shipping-methods`).
5. Enable Novaposhta address field for user
   profile (`/admin/config/people/profile-types/manage/PROFILE_TYPE`) ->
   Profiles of this type includes Novaposhta address

## Maintainers

- Oleksandr
  Yushchenko - [Oleksandr Yushchenko](https://www.drupal.org/u/oleksandr-yushchenko)
- Mykhailo Gurei - [ozin](https://www.drupal.org/u/ozin)
