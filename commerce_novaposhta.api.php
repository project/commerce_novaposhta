<?php

/**
 * @file
 * Hooks for the commerce_novaposhta module.
 */

use Drupal\commerce_novaposhta\Service\NovaposhtaManagerInterface;

/**
 * @addtogroup hooks
 * @{
 */

/**
 * Alter request data.
 */
function hook_commerce_novaposhta_request_data_alter(&$data) {
  if ($data['modelName'] == NovaposhtaManagerInterface::ADDRESS_MODEL) {
    $data['calledMethod'] = NovaposhtaManagerInterface::GET_CITIES_METHOD;
  }
}

/**
 * Alter response results.
 */
function hook_commerce_novaposhta_result_data_alter(&$result, $request_data) {
  if ($request_data['modelName'] == NovaposhtaManagerInterface::ADDRESS_MODEL) {
    $result = [];
  }
}

/**
 * @} End of "addtogroup hooks".
 */
